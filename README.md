## 采用Server-Sent Events（SSE）实现代码打字机效果示例

| 端 | 技术栈 | 技术点 |
| -- | ----- | ----- |
| 后端 | node | event-stream |
| 后端 | js | EventSource |

> 参考： <br>
> [Server-Sent Events 教程](https://www.ruanyifeng.com/blog/2017/05/server-sent_events.html) <br>
> [EventSource 浏览器支持查询](https://caniuse.com/?search=eventsource)

## node服务采用vercel部署
>
> package.json中启动生产服务命令是：npm run start,换成npm run build会出现一直部署中<br>
> 需要安装@vercel/node，并在node服务根路径下建 vercel.json进行配置
