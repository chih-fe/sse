var http = require("http");


var code = `const http = require('http');↵
const axios = require('axios');↵
const API_KEY = '<YOUR API KEY HERE>';↵
const server = http.createServer((req, res) => {↵
  res.writeHead(200, {↵
    'Content-Type': 'text/event-stream',↵
    'Cache-Control': 'no-cache',↵
    'Connection': 'keep-alive'↵
  });↵

  setInterval(async () => {↵
    try {↵
      const response = await axios.post('https://api.openai.com/v1/engines/davinci/jobs', {↵
        prompt: "What's the current time?"↵
      }, {↵
        headers: {↵
          'Authorization': \`Bearer $\{API_KEY\}\`,↵
          'Content-Type': 'application/json'↵
        }↵
      });↵

      res.write(\`data: $\{response.data.choices[0].text\}\\n\\n\`);↵
    } catch (error) {↵
      console.error(error);↵
    }↵
  }, 1000);↵
});↵

server.listen(3000);↵
`

http.createServer(function (req, res) {
  var fileName = "." + req.url;

  if (fileName === "./stream") {
    res.writeHead(200, {
      "Content-Type":"text/event-stream",
      "Cache-Control":"no-cache",
      "Connection":"keep-alive",
      "Access-Control-Allow-Origin": '*',
    });
    res.write(": this is a test stream\n\n")
    res.write("retry: 10000\n");
    res.write("event: connecttime\n");
    res.write("data: " + (new Date()) + "\n\n");


    var length = code.length
    var start = 0
    interval = setInterval(function () {
        if(start>=length-1) {
            clearInterval(interval);
            return
        }
        res.write("id: " + (new Date().getTime()) + "\n\n")
        res.write("data: " + code[start] + "\n\n");
        start++
    }, 10);

    req.connection.addListener("close", function () {
      clearInterval(interval);
    }, false);
  } else {
    res.writeHead(200, {'Content-Type': 'text/plain'});

    // 发送响应数据 "Hello World"
    res.end('this is about server-sent events example\n');
  }
}).listen(8844, '127.0.0.1');


// const http = require('http');
// const axios = require('axios');

// const API_KEY = '<YOUR API KEY HERE>';

// const server = http.createServer((req, res) => {
//   res.writeHead(200, {
//     'Content-Type': 'text/event-stream',
//     'Cache-Control': 'no-cache',
//     'Connection': 'keep-alive'
//   });

//   setInterval(async () => {
//     try {
//       const response = await axios.post('https://api.openai.com/v1/engines/davinci/jobs', {
//         prompt: "What's the current time?"
//       }, {
//         headers: {
//           'Authorization': `Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik1UaEVOVUpHTkVNMVFURTRNMEZCTWpkQ05UZzVNRFUxUlRVd1FVSkRNRU13UmtGRVFrRXpSZyJ9.eyJodHRwczovL2FwaS5vcGVuYWkuY29tL3Byb2ZpbGUiOnsiZW1haWwiOiJwcnoxOTkxNDEyQDEyNi5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZ2VvaXBfY291bnRyeSI6IlVTIn0sImh0dHBzOi8vYXBpLm9wZW5haS5jb20vYXV0aCI6eyJ1c2VyX2lkIjoidXNlci1xek0yTVhsZmsyckQ2dEo1SVlNSkkzSUYifSwiaXNzIjoiaHR0cHM6Ly9hdXRoMC5vcGVuYWkuY29tLyIsInN1YiI6Imdvb2dsZS1vYXV0aDJ8MTA3NTI5NTIzNzY0MDI2NjMyOTYyIiwiYXVkIjpbImh0dHBzOi8vYXBpLm9wZW5haS5jb20vdjEiLCJodHRwczovL29wZW5haS5vcGVuYWkuYXV0aDBhcHAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTY3NTIxODAxOSwiZXhwIjoxNjc1ODIyODE5LCJhenAiOiJUZEpJY2JlMTZXb1RIdE45NW55eXdoNUU0eU9vNkl0RyIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwgbW9kZWwucmVhZCBtb2RlbC5yZXF1ZXN0IG9yZ2FuaXphdGlvbi5yZWFkIG9mZmxpbmVfYWNjZXNzIn0.hNQVkRJo_mupqn_vHhGxqZ2qoVr6lViWkgqX_epomBuTC43J1oudj0oibIt0AATxtIK6WbE41puwwbuYFLrZ61izn_mjSfoeIjBHfewld6eR56zSDMJOSpAY-ayXEaP0WzrujRIlhattA6WGHqbe2AC3KVAo_cI25SSmG4LIQFRUCJ3UdFG_qsRmajhISR7rl_CNqXKgxkPQxkUe1mYpALu7TrZIKtfBlXEJqmtI9yzqZxVk80cKWbQmrIsxB3ErBhikBfKSnPrcjwg5O5sGsmX3tQCVwRkGcLZzfSy1cEWoptkrr4CvXJ-6V2G85F6sf3CTCuGXqhetkU39J_pWxQ`,
//           'Content-Type': 'application/json'
//         }
//       });

//       res.write(`data: ${response.data.choices[0].text}\n\n`);
//     } catch (error) {
//       console.error(error.response);
//     }
//   }, 1000);
// });

// server.listen(8844);
