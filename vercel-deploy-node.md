## 官网

[Vercel 官网](https://vercel.com/)

[Vercel 工作流官网（网页效果炫酷）](https://vercel.com/workflow)

## 常见命令行

将 Vercel 下载到全局（npm i vercel -g），不知道有什么命令就-h

### vercel常见命令

- `vercel login`：登录 Vercel 账号
- `vercel dev`：本地开启服务
- `vercel dev --bug`：本地开启服务并打印日志
- `vercel`：部署本地资源到 - Vercel 上
- `vercel --prod`：更新本地网页
- vercel 可以用 vc 来代替，vc 是 Vercel 的缩写

## 部署静态服务

我们现在已经对 vercel 有所了解，前文中说到 Vercel 能简化开发者部署服务，那它能简化到什么程度呢？

这里我们从零部署一个简易静态服务

本地安装 Vercel

```
npm i vercel -g
```

登录 Vercel

```
vc login
```

选择好连接的方式后，会在网站弹出

vercel 登录成功

创建一个HTML文件，后续我们要将其上传至 Vercel 服务器上

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vercel Demo</title>
</head>

<body>
    <h1>Vercel Demo</h1>
</body>

</html>

```

本地测试一番，输入命令行

```
vc dev
```

因为我们这是第一次执行，根目录下没有.vercel，所以要填写一些必要信息，这时你的本地和 Vercel 服务器就绑定好了

部署服务

```
vc
```

在 <https://vercel-xxx.ver>... 中能访问到我们的静态服务

在截图中我们也看到了这句话`Deployed to production. Run vercel --prod to overwrite later`，后续我们要更新资源，用 `vercel --prod` 即可

好了，除去必要的登录，我们就用了三个命令就把本地服务部署到 Vercel 服务器上

- `vercel dev` ：开发时使用，查看应用是否跑得起来
- `vercel`：部署服务
- `vercel --prod`：更新应用（资源）
可以登录 Vercel 后台查看部署情况

## 部署 Node 服务

回归主题，最终我们想部署的是 Node 服务，是后端服务，而非前端静态资源服务，这是关键

同样建立新项目

```bash
mkdir vercel-koa2
cd vercel-koa2
npm init -y
npm i koa -S
touch index.js
```

编写 index.js 中的内容

```js
const Koa = require('koa');
const app = new Koa();

app.use(async ctx => {
    ctx.body = 'Hello Vercel';
});

app.listen(3008, () => {
    console.log('3008项目启动')
});
```

PS： 3000端口默认会被 Vercel 使用，所以 Koa 服务要换个端口
使用命令`vercel dev`

于是乎我们部署它

vercel
vercel 部署失败

搞半天没部署上去，后台查看也是无果，呜呼悲哉

google后，发现原来还有一个 `vercel.json`，可以用 vercel.json 配置和覆盖 vercel 默认行为

下载 `@vercel/node` 包

```
npm i @vercel/node -S
```

填写配置：

```json
{
  "version": 2,
  "builds": [
    {
      "src": "index.js",
      "use": "@vercel/node"
    }
  ]
}
```

执行 vercel 部署服务

至此，就完成了 Koa 服务的部署

与部署静态资源多了两个步骤

下载 `@vercel/node` 和配置 `vercel.json`
